import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by fcek2 (Fran�ois Kiene) on 08/02/2017.
 */

/*
 Class representing the Depth-limited Search Algorithm
 */
public class DepthLimitedSearch {

    private String fileName;
    private Puzzle start;
    private Puzzle end;

    // Constructor that backs up the file name, the start and end puzzle.
    public DepthLimitedSearch(Puzzle start, Puzzle end) {
        this.fileName = start.puzzle+"2"+end.puzzle+".txt";
        this.start = start;
        this.end = end;
    }

    // Resolve the problem by calling the finder and writeFile method
    public void resolve()
    {
        List<Puzzle> tmpList;

        tmpList = finder(this.start, this.end, (byte)24, (byte)-1);
        if (tmpList != null)
        {
            Collections.reverse(tmpList);
            writeFile(tmpList);
        }
    }

    /*
     Move the blank tile in 4 directions, except the previous one.
     Return the 3 puzzle created
     */
    private List<Puzzle> move(Puzzle puzzle, byte prevDir)
    {
        List<Puzzle> tmpList = new ArrayList<>();
        byte dir = 0;
        Puzzle tmpPuzzle;

        while (dir < 4)
        {
            if (prevDir != dir && (tmpPuzzle = new Puzzle(puzzle).move(dir)) != null)
                tmpList.add(tmpPuzzle);
            dir++;
        }

        return tmpList;
    }

    // Recursive method that tries to find the solution by moving recursively in all directions.
    private List<Puzzle> finder(Puzzle start, Puzzle end, byte depthSize, byte prevDir)
    {
        // Algorithm End Condition
        if (start.puzzle.equals(end.puzzle))
        {
            List<Puzzle> tmpList = new LinkedList<>();
            tmpList.add(new Puzzle(start));
            return (tmpList);
        }
       else if (depthSize == 0)
           return (null);

        List<Puzzle> moves = move(start, prevDir);
        //Recursive call in each direction
        for (Puzzle move : moves)
        {
            List<Puzzle> next = finder(move, end, (byte)(depthSize - 1), move.dir);
            if (next != null)
            {
                // Recursively add solutions
                next.add(start);
                return (next);
            }
        }
        return (null);
    }

    // Class to write all the puzzle solution in the txt file
    private void writeFile(List<Puzzle> all) {
        int k = 4;
        int f = 0;
        int j;

        try{
            PrintWriter writer = new PrintWriter("./"+this.fileName, "UTF-8");
            for (int i = 0; i < 4; i++) {
                for (Puzzle puzzle : all) {
                    j = f;
                    while (j < k) {
                        writer.printf("%c", puzzle.puzzle.charAt(j));
                        j++;
                    }
                    if (all.indexOf(puzzle) != (all.size() -1))
                        writer.printf(" ");
                }
                f += 4;
                k += 4;
                if (i < 3)
                    writer.printf("\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
