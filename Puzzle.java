/**
 * Created by fcek2 (Fran�ois Kiene) on 08/02/2017.
 */

/*
 Class that handles the puzzle and moves the blank tile.
 The 4 by 4 puzzle is represented in a String.
 And the position of the blank tile is stored in a byte.
 */
public class Puzzle {
    public static byte North = 0;
    public static byte East = 1;
    public static byte South = 2;
    public static byte West = 3;

    public String puzzle;
    public byte dir;
    private byte blankTile;

    private byte height;
    private byte width;

    // Constructor that creates the puzzle from: string, height and width received as parameters
    public Puzzle(String puzzle, byte height, byte width)
    {
        this.puzzle = puzzle;
        this.height = height;
        this.width = width;
        this.blankTile = (byte)puzzle.indexOf("_");
    }

    // Constructor used to make a copy of a puzzle
    public Puzzle(Puzzle puzzle)
    {
        this.puzzle = puzzle.puzzle;
        this.blankTile = puzzle.blankTile;
        this.height = puzzle.height;
        this.width = puzzle.width;
    }

    // Check, move and swap the blank tile from the positions given in parameter
    public String moveTile(byte blankTile, byte tmpBlankTile)
    {
        char[] tmpPuzzle = puzzle.toCharArray();

        if (tmpPuzzle[tmpBlankTile] == '+')
            return (null);
        char tmp = tmpPuzzle[blankTile];
        tmpPuzzle[blankTile] = tmpPuzzle[tmpBlankTile];
        tmpPuzzle[tmpBlankTile] = tmp;

        return (new String(tmpPuzzle));
    }

    // Check and move the blank tile in the given direction as a parameter.
    public Puzzle move(byte dir)
    {
        byte tmpBlankTile;

        // Check if the blank tile can move and create a temporary blank title if it can
        if (dir == North && !(this.blankTile / this.width == 0))
            tmpBlankTile = (byte)(this.blankTile - this.width);
        else if (dir == South && !(this.blankTile / this.width == this.height - (byte)1))
            tmpBlankTile = (byte)(this.blankTile + this.width);
        else if (dir == East && !(this.blankTile % this.width == this.width - (byte)1))
            tmpBlankTile = (byte)(this.blankTile + 1);
        else if (dir == West && !(this.blankTile % this.width == 0))
            tmpBlankTile = (byte)(this.blankTile - 1);
        else
            return (null);

        // Check if the blank tile can swap (character '+')
        if (tmpBlankTile >= 0 && (this.puzzle = moveTile(this.blankTile, tmpBlankTile)) != null)
            this.blankTile = tmpBlankTile;
        else
            return (null);

        // Save previous direction for the next move
        if (dir < 2)
            this.dir = (byte)(dir + 2);
        else
            this.dir = (byte)(dir - 2);
        return (this);
    }
}
