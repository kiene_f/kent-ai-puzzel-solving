/**
 * Created by fcek2 (Fran�ois Kiene) on 08/02/2017.
 */

public class Main {

    public static void main(String[] args) {

        String[] puzzles = {
                "da_c+dbd+dba++bd2ddcd+_ab+bab++dd.txt",
                "dbdc+ab_+bdd++da2dbdb+cda+ab_++dd.txt",
                "dddd+cb_+bda++ab2dddd+cba+bdb++_a.txt",
                "ddda+bdc+b_d++ab2ddda+bdc+ba_++bd.txt",
                "ddda+bdc+ba_++bd2ddac+bd_+dba++bd.txt",
                "daab+bdb+dd_++dc2dadb+bd_+adc++bd.txt",
                "ddbc+_da+bdb++ad2dbdb+d_c+dab++ad.txt",
                "dabc+ddb+d_a++bd2dabc+ddd+abd++_b.txt",
                "dbdc+bdb+dda++a_2ddcb+bd_+bdd++aa.txt",
                "dbc_+dbd+adb++ad2dcdb+bbd+d_a++da.txt",
                "ddbc+d_b+ada++bd2dbdd+dbc+aa_++bd.txt",
                "dcdd+ab_+bbd++da2db_a+bdd+dac++db.txt",
                "dacb+d_b+bdd++ad2ddbc+a_b+bad++dd.txt",
                "dbdb+dad+d_c++ba2dbad+bdd+d_b++ac.txt",
                "d_bd+ada+bbc++dd2dbda+_dc+abd++bd.txt",
                "dc_d+dbb+abd++ad2dbdd+dca+ba_++db.txt"
        };

        for (String puzzle : puzzles)
        {
            Puzzle start = new Puzzle(puzzle.substring(0, 16), (byte)4, (byte)4);
            Puzzle end = new Puzzle(puzzle.substring(17, 33), (byte)4, (byte)4);

            DepthLimitedSearch depth = new DepthLimitedSearch(start, end);
            depth.resolve();
        }
    }
}
