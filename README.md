# Kent - AI - Puzzel Solving #
### French

Programme permettant de r�soudre un puzzle � l'aide de l'algorithm "Iterative deepening depth limited search" dans le cadre de mon module "CO528 Introduction to Intelligent Systems"
![alt text](http://francois.kiene.fr/assets/images/works/puzzle.jpg)

## Built With

* Java
* Iterative deepening depth limited search